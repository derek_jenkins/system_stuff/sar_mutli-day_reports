monthly_report() {
    #  this creates a report (such as /tmp/sar_reports/cpu_report.out) for each relevant file in /var/log/sa.
    #  obviously, you can update the array to have whichever sar flags you want.
    [[ -e /tmp/sar_reports ]] || mkdir /tmp/sar_reports
    pushd /var/log/sa >/dev/null
    declare -A SAR_DICT=( ["u"]="cpu" ["r"]="memory" ["d"]="disk" )
    for SAR_TYPE in "${!SAR_DICT[@]}"; do
        for FILE in sa??; do
            sar -"${SAR_TYPE}" -f "${FILE}" >> /tmp/sar_reports/"${SAR_DICT[$SAR_TYPE]}"_report.out
        done
    done
    popd >/dev/null
}